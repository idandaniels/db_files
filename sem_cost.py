import pandas as pd
import calendar
import datetime
import sys
import pandas_gbq

# define paths - REMEMBER TO UPDATE THE FOLDER WHEN CHANGING YEAR OF REPORTS
draw_path = r'C:\Users\idan\Dropbox (Tripbase)\Cruise Report\Daily Reports 2020\ReportsData\\'
save_path = r'C:\Users\idan\OneDrive\Desktop\db files\\'

def source_corrector(val):
    if val.lower() == 'bing':
        return 'msn'
    else:
        return val;

def vertical_corrector(cols):
    #print('im inside the vertical corrector function...')
    site = cols[0]
    vertical = cols[1]

    # print('im in %s, %s' %(site, vertical))
    if (site.lower() == 'top-cruise-deals' or site.lower() == 'cruise-compare') and vertical == 'None':
        print('vertical was changed to cruise')
        return 'cruise'
    elif site.lower() == 'cheap-auto-rentals':
        print('vertical was changed to cars')
        return 'cars'
    elif vertical == 'None':
        print('vertical was changed to %s ' % site.split('-')[1].lower())
        return site.split('-')[1].lower()
    elif vertical == 'cruises':
        print('vertical was changed from cruises to cruise')
        return 'cruise'
    else:
        return vertical.lower()

def site_corrector(site):
    if (site.lower() == 'cheap-auto-rentals' or site.lower() == 'cruise-compare' or site.lower() == 'top-cruise-deals'):
        return site.lower()
    elif len(site) == 1:
        return site.lower()
    else:
        return site.split('-')[0].lower()

# checking rows total numbers
def check_total_rows_num():
    total_rows = sem_costs_df.shape[0]
    if old_df_rows + new_df_rows == total_rows:
        print('Number of rows in the old df: %d' % old_df_rows)
        print('Number of rows in the new df: %d' % new_df_rows)
        print('Number of rows in the final df: %d' % total_rows)
        print('rows are equal, continue with the program')
    else:
        print('Rows are not equal')
        print('/n')
        print('Number of rows in the old df: %d' % old_df_rows)
        print('Number of rows in the new df: %d' % new_df_rows)
        print('Number of rows in the final df: %d' % total_rows)
        exit()


# loading the old dataframe
old_df = pd.read_csv(save_path + 'sem_costs.csv')

# uploading the excel file with the tabs
xls = pd.ExcelFile(draw_path + '03-20_sem_costs.xlsx')
sheet_to_df_map = []
for sheet_name in xls.sheet_names:
    sheet_to_df_map.append(xls.parse(sheet_name))

# check length of sheets names and number of sheets
try:
    sheets_len = len(xls.sheet_names)
    num_of_actual_sheets = len(sheet_to_df_map)
    if sheets_len == num_of_actual_sheets:
        print('Sheets numbers are equal with the sheets titles length which is %d' %num_of_actual_sheets)
    else:
        print('Numbers are not equal')
        if num_of_actual_sheets > sheets_len:
            print('There are more sheets then sheets titles... please check if there is a sheet without a title')
        elif sheets_len > num_of_actual_sheets:
            print('There are more titles then sheets... please check again')
        else:
            print('Couldnt understand what the problem is')
        sys.exit()
except Exception as e:
    print(str(e))

# working on the new data frame, columns standardization and date time objects
final_df = pd.DataFrame()
for i, sheet in enumerate(sheet_to_df_map):
    name_of_df = xls.sheet_names[i]
    sheet['platform'] = name_of_df
    if name_of_df == 'Adwords':
        sheet['date'] = pd.to_datetime(sheet['date'])
    elif name_of_df == 'Bing':
        sheet.rename(columns={'Date': 'date', '45Off-Flights': 'site', 'spend': 'cost', 'Account name': 'site'}, inplace=True)
        sheet['date'] = pd.to_datetime(sheet['date'])
    elif name_of_df == 'Gemini':
        sheet.rename(columns={'Date': 'date', 'Account': 'site', 'spend': 'cost'}, inplace=True)
        sheet['date'] = pd.to_datetime(sheet['date'])
    elif name_of_df == 'Facebook':
        sheet['date'] = pd.to_datetime(sheet['date'])
        sheet.rename(columns={'Cost': 'cost'}, inplace=True)
    elif name_of_df == 'Taboola':
        sheet.rename(columns={'Date': 'date', 'Account name': 'site', 'spend': 'cost'}, inplace=True)
        sheet['date'] = pd.to_datetime(sheet['date'])
    elif name_of_df == 'Yandex':
        sheet.rename(columns={'Date': 'date', 'Account name': 'site', 'spend': 'cost'}, inplace=True)
        sheet['date'] = pd.to_datetime(sheet['date'])

    # appending all dataframes together
    final_df = final_df.append(sheet, sort=True)

# handle all null values with fillna
#final_df[['Clicks', 'cost']] = final_df[['Clicks', 'cost']].fillna(value=0)
final_df['cost'] = final_df['cost'].fillna(value=0)
final_df[['vertical', 'site']] = final_df[['vertical', 'site']].fillna(value='None')

# rename platform to source
final_df.rename(columns={'platform': 'source'}, inplace=True)

# create current month, year and last day of month variables
current_month = final_df['date'].iloc[0].month
print('Current month is %d ' % current_month)
year = final_df['date'].iloc[0].year
print('Current year %d ' % year)
last_day_of_month = calendar.monthrange(year, current_month)[1]
print('Last day of month is %d ' % last_day_of_month)

# create variables of first and last day of current month
first_day_of_current_month = datetime.datetime(year, current_month, 1)
print('First day of current month %s ' % first_day_of_current_month)
last_day_of_current_month = datetime.datetime(year, current_month, last_day_of_month)
print('Last day of current month %s ' % last_day_of_current_month)

# count number of non relevant dates
dates_greater_than_current_month = final_df['date'][final_df['date'] > last_day_of_current_month].count()
dates_less_than_current_month = final_df['date'][final_df['date'] < first_day_of_current_month].count()


# creating a function that return correct date or applying not relevant date
def validate_month(date):
    try:
        month = date.month
        if month == current_month:
            return date
        else:
            return 'not relevant date'
    except Exception as ex:
        print(str(ex))


#final_df['date'] = final_df['date'].apply(validate_month)
print('#########################################################################################################')
#print(final_df[final_df['date'] == 'not relevant date'])

try:
    number_of_not_relevant_dates = final_df['date'][final_df['date'] == 'not relevant date'].count()
except Exception as e:
    print(str(e))
    print('Set the number to 0 and continue with program...')
    number_of_not_relevant_dates = 0
    print('Number of not relevant dates on this table is %d' % number_of_not_relevant_dates)



# comparing dates before deleting them
print('Number of rows when date is greater then this month (%d): %d' % (current_month, dates_greater_than_current_month))
print('***')
print('Number of rows when date is smaller then this month (%d): %d' % (current_month, dates_less_than_current_month))
print('***')
print('Number of total rows with not relevant date, after applying date validation function: %d' %number_of_not_relevant_dates)
print('***')
print('Comparing the numbers...')
# if (dates_greater_than_current_month + dates_less_than_current_month) == number_of_not_relevant_dates:
#     print('The numbers are equal')
# else:
#     print('There is a mismatched number... please check the numbers again')
#     print('Exiting the program...')
#     sys.exit()

# clear the data frame from non relevant dates
if number_of_not_relevant_dates != 0:
    final_df = final_df[final_df['date'] != 'not relevant date']

# assigning back the date time object on both data frames
if 'str' in str(type(old_df['date'].iloc[0])):
    old_df['date'] = pd.to_datetime(old_df['date'])

if 'str' in str(type(final_df['date'].iloc[0])):
    final_df['date'] = pd.to_datetime(final_df['date'])

# Fixing last format issues
#final_df.rename(columns={'Clicks': 'clicks'}, inplace=True)
final_df['vertical'] = final_df[['site', 'vertical']].apply(vertical_corrector, axis=1)
final_df['site'] = final_df['site'].apply(site_corrector)
final_df['source'] = final_df['source'].apply(lambda x: x.lower())

# Adding a clicks column with 0 values
final_df['clicks'] = 0

# override the current month variable to the old_df current date
# current_month = old_df['date'].iloc[0].month

# clear non relevant dates from old_df in there are any
# old_df['date'] = old_df['date'].apply(validate_month)
#
# old_df = old_df[old_df['date'] != 'not relevant date']

# Prepare the date for updating previous dates with new data,
# if no previous date with new data, the data frame will stay unique as the old df
# preparing the shape of each df to be concatenated
old_df_without_updated_records = old_df[~old_df['date'].isin(final_df['date'])]
old_df_rows = old_df_without_updated_records.shape[0]
new_df_rows = final_df.shape[0]
sem_costs_df = pd.concat([old_df_without_updated_records, final_df], sort=True).sort_values(by='date', ascending=False)

# applying source corrector
sem_costs_df['source'] = sem_costs_df['source'].apply(source_corrector)

# checking number of rows from both dataframes
check_total_rows_num()

# saving the final file
sem_costs_df.to_csv(save_path + 'sem_costs.csv', index=False)
#sem_costs_df.to_csv(save_path + 'sem_costs_test_idan.csv', index=False)

# saving a backup file
sem_costs_df.to_csv(save_path + 'sem_costs_backup.csv', index=False)

# uploading a backup file to BigQuery
proj_id = 'daily-reports-252312'
table_id = 'backup.sem_cost_backup'
sem_costs_df.to_gbq(table_id, project_id=proj_id, if_exists='replace')

