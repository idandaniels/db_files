import pandas as pd
import pandas_gbq
import os

# checking rows total numbers
def check_total_rows_num():
    total_rows = data_rev_df.shape[0]
    if old_df_rows + new_df_rows == total_rows:
        print('Number of rows in the old df: %d' % old_df_rows)
        print('Number of rows in the new df: %d' % new_df_rows)
        print('Number of rows in the final df: %d' % total_rows)
        print('rows are equal, continue with the program')
    else:
        print('Rows are not equal')
        print('/n')
        print('Number of rows in the old df: %d' % old_df_rows)
        print('Number of rows in the new df: %d' % new_df_rows)
        print('Number of rows in the final df: %d' % total_rows)
        exit()

def correct_source(val):
    if 'yandex' in val.lower():
        return 'yandex'
    elif 'newsletter' in val.lower():
        return 'newsletter'
    elif 'facebook' in val.lower():
        return 'facebook'
    elif 'none' in val.lower():
        return 'none'
    elif val.lower() == 'bing':
        return 'msn'
    elif val.lower() == 'google':
        return 'adwords'
    elif '_bwe' in val.lower():
        return 'adwords'
    else:
        return val


def site_corrector(site):
    if site == 'last_min_flights':
        site = 'lastmin'
    elif '_' in site:
        site = site.replace('_', '-')

    return site

def vertical_corrector(vertical):
    if 'cruises' in vertical.lower():
        return 'cruise'
    elif 'car' in vertical.lower():
        return 'cars'
    elif vertical.lower() == 'tripbases':
        return 'flights'
    elif 'flights' in vertical.lower():
        return 'flights'
    else:
        return vertical

draw_path = r'C:\Users\idan\Dropbox (Tripbase)\Cruise Report\Daily Reports 2020\ReportsData\\'
#draw_path = r'C:\Users\idan\OneDrive\Desktop\db files\\'
save_path = r'C:\Users\idan\OneDrive\Desktop\db files\\'

old_df = pd.read_csv(save_path + 'final_file_rev.csv')



# define the new df, it depend on a file,
# could be a file with the same month's name or the next month of the current file
# pay attention to the draw path, it will be changed from year to year. remember to choose the relevant year every time

new_df = pd.read_csv(draw_path + '03-20_data-revenue.csv')

# transfer date to date time object
old_df['date'] = pd.to_datetime(old_df['date'])
new_df['date'] = pd.to_datetime(new_df['date'])

# Prepare the date for updating previous dates with new data,
# if no previous date with new data, the data frame will stay unique as the old df
# preparing the shape of each df to be concatenated
old_df_without_updated_records = old_df[~old_df['date'].isin(new_df['date'])]
old_df_rows = old_df_without_updated_records.shape[0]
new_df_rows = new_df.shape[0]
data_rev_df = pd.concat([old_df_without_updated_records, new_df], sort=True).sort_values(by='date', ascending=False)

# call the check rows function that makes sure that the rows are equal - the old and the new df
check_total_rows_num()

# filling na values
data_rev_df['vertical'] = data_rev_df['vertical'].fillna(value="no vertical")
data_rev_df['vertical'] = data_rev_df['vertical'].apply(vertical_corrector)
data_rev_df['source'] = data_rev_df['source'].fillna(value="no source")
data_rev_df['advertiser'] = data_rev_df['advertiser'].fillna(value="no advertiser")
data_rev_df['advertiser'] = data_rev_df['advertiser'].apply(lambda x: x.lower())

data_rev_df['source'] = data_rev_df['source'].apply(correct_source)
data_rev_df['source'] = data_rev_df['source'].apply(lambda x: x.lower())
data_rev_df['site'] = data_rev_df['site'].apply(site_corrector)

data_rev_df.to_csv(save_path + 'final_file_rev.csv', index=False)

# saving a backup file
data_rev_df.to_csv(save_path + 'final_file_rev_backup.csv', index=False)

# updating a backup table in big query
proj_id = 'daily-reports-252312'
table_id = 'backup.data_rev_backup'
data_rev_df.to_gbq(table_id, project_id=proj_id, if_exists='replace')
