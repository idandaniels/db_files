import pandas as pd
import calendar
import datetime
import sys
import pandas_gbq
import os.path

draw_path = r'C:\Users\idan\Dropbox (Tripbase)\Cruise Report\Daily Reports 2020\ReportsData\\'
save_path = r'C:\Users\idan\OneDrive\Desktop\db files\\'

#creating a function that return correct date or applying not relevant date
def validate_month(date):
    try:
        month = date.month
        if month == current_month:
            return date
        else:
            return 'not relevant date'
    except Exception as e:
        print(str(e))

# checking rows total numbers
def check_total_rows_num():
    total_rows = final_adsense_df.shape[0]
    if old_df_rows + new_df_rows == total_rows:
        print('Number of rows in the old df: %d' % old_df_rows)
        print('Number of rows in the new df: %d' % new_df_rows)
        print('Number of rows in the final df: %d' % total_rows)
        print('rows are equal, continue with the program')
    else:
        print('Rows are not equal')
        print('/n')
        print('Number of rows in the old df: %d' % old_df_rows)
        print('Number of rows in the new df: %d' % new_df_rows)
        print('Number of rows in the final df: %d' % total_rows)
        exit()

def vertical_seperator(phrase):
    if 'cruise-' in phrase.lower():
        return 'cruise'
    elif 'auto' in phrase.lower():
        return 'cars'
    else:
        return phrase.split('-')[len(phrase.split('-')) - 1].lower()

def site_rename(phrase):
    if '45off-cruise' in phrase.lower():
        return '45off'
    elif 'cruise' in phrase.lower():
        return phrase.lower()
    elif 'auto' in phrase.lower():
        return phrase.lower()
    else:
        return phrase.split('-')[0].lower()


# loading the files
old_df = pd.read_csv(save_path + 'adsense_rev.csv')
adsense_df = pd.read_excel(draw_path + '03-20-adsense_revenue.xlsx')

#drop unnamed columns
adsense_df.drop(adsense_df.columns[adsense_df.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)

#rename spend column
adsense_df.rename(columns={'spend': 'revenue', 'Date': 'date'}, inplace=True)

# replace nan values with 0
adsense_df['revenue'].fillna(value=0, inplace=True)

adsense_df['vertical'] = adsense_df['Account name'].apply(vertical_seperator)
adsense_df['site'] = adsense_df['Account name'].apply(site_rename)
adsense_df.drop(columns='Account name', inplace=True)
adsense_df['date'] = pd.to_datetime(adsense_df['date'])

# create current month, year and last day of month variables
current_month = adsense_df['date'].iloc[0].month
year = adsense_df['date'].iloc[0].year
last_day_of_month = calendar.monthrange(year, current_month)[1]

# create variables of first and last day of current month
first_day_of_current_month = datetime.datetime(year, current_month, 1)
last_day_of_current_month = datetime.datetime(year, current_month, last_day_of_month)
dates_greater_than_current_month = adsense_df['date'][adsense_df['date'] > last_day_of_current_month].count()
dates_less_than_current_month = adsense_df["date"][adsense_df['date'] < first_day_of_current_month].count()

print('The number of dates with records over the next month is: %d' % dates_greater_than_current_month)
print('The number of dates with records over the previous month is: %d' % dates_less_than_current_month)

# validating the month on the records
adsense_df['date'] = adsense_df['date'].apply(validate_month)
try:
    number_of_not_relevant_dates = adsense_df['date'][adsense_df['date'] == 'not relevant date'].count()
except Exception as e:
    print(str(e))
    print('Set the number to 0 and continue with program...')
    number_of_not_relevant_dates = 0
    print('Number of not relevant dates on this table is %d' %number_of_not_relevant_dates)

# comparing dates before deleting them
print('Number of rows when date is greater then this month (%d): %d' %(current_month, dates_greater_than_current_month))
print('***')
print('Number of rows when date is smaller then this month (%d): %d' %(current_month, dates_less_than_current_month))
print('***')
print('Number of total rows with not relevant date, after applying date validation function: %d' %number_of_not_relevant_dates)
print('***')
print('Comparing the numbers...')
if (dates_greater_than_current_month + dates_less_than_current_month) == number_of_not_relevant_dates:
    print('The numbers are equal')
else:
    print('There is a mismatched number... please check the numbers again')
    print('Exiting the program...')
    sys.exit()

# clear the data frame from non relevant dates
# checking before if there are any not relevant dates
if number_of_not_relevant_dates != 0:
    adsense_df = adsense_df[adsense_df['date'] != 'not relevant date']

adsense_df['date'] = pd.to_datetime(adsense_df['date'])
old_df['date'] = pd.to_datetime(old_df['date'])

# Prepare the date for updating previous dates with new data,
#if no previous date with new data, the data frame will stay unique as the old df
#preparing the shape of each df to be concatenated

old_df_without_updated_records = old_df[~old_df['date'].isin(adsense_df['date'])]
old_df_rows = old_df_without_updated_records.shape[0]
new_df_rows = adsense_df.shape[0]
final_adsense_df = pd.concat([old_df_without_updated_records, adsense_df], sort=True).sort_values(by='date', ascending=False)

# print total rows check between both data frames
check_total_rows_num()

# saving the final file
final_adsense_df.to_csv(save_path + 'adsense_rev.csv', index=False)

# saving a backup file
final_adsense_df.to_csv(save_path + 'adsense_rev_backup.csv', index=False)

# updating a backup file in bigQuery
proj_id = 'daily-reports-252312'
table_id = 'backup.final_adsense_backup'
final_adsense_df.to_gbq(table_id, project_id=proj_id, if_exists='replace')

