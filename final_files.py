import pandas as pd
import pandas_gbq
import datetime
import sys
import os.path

def valid_rows(local_shape, bq_shape):
    try:
        if local_shape == bq_shape:
            print('Rows are equal %d' %local_shape)
        else:
            print('Rows are not equal, please where the problem came from...')
            print('local file rows: %d, bq file rows: %d' % (local_shape, bq_shape))
    except Exception as e:
        print(str(e))


#### duplicates rows validation
def dup_rows_validation(query, table_name, project_id):
    try:
        num_of_dup_rows = pandas_gbq.read_gbq(query, project_id=project_id)
        if num_of_dup_rows.shape[0] > 0:
            print('There are %d duplicate rows in table %s' % (num_of_dup_rows.shape[0], table_name))
            print(num_of_dup_rows)
        else:
            print('There are 0 duplicated rows!')
    except Exception as e:
        print(str(e))


path = r'C:\Users\idan\OneDrive\Desktop\db files\\'

data_rev_df = pd.read_csv(path + 'final_file_rev.csv')
sem_costs_df = pd.read_csv(path + 'sem_costs.csv')
adsense_rev_df = pd.read_csv(path + 'adsense_rev.csv')

# Making sure date is datetime object
sem_costs_df['date'] = pd.to_datetime(sem_costs_df['date'])
data_rev_df['date'] = pd.to_datetime(data_rev_df['date'])
adsense_rev_df['date'] = pd.to_datetime(adsense_rev_df['date'])

# Creating first union - data_rev and adsense
union_datarev_adsense = pd.concat([data_rev_df, adsense_rev_df], sort=True)
datarev_grouped_adsense = union_datarev_adsense.groupby(by=['date', 'site', 'vertical']).agg({'revenue':'sum'}).reset_index()

# Creating the union of datarv + adsense and sem_costs
datarev_sem_adsense_union = pd.concat([datarev_grouped_adsense, sem_costs_df], sort=True)
final_cost_rev_df = datarev_sem_adsense_union.groupby(by=['date', 'site', 'vertical']).agg({'revenue':'sum', 'cost':'sum'}).reset_index()

# creating day column
dmap = {0: 'Mon', 1: 'Tue', 2: 'Wed', 3: 'Thu', 4: 'Fri', 5: 'Sat', 6: 'Sun'}
final_cost_rev_df['day'] = final_cost_rev_df['date'].apply(lambda x: x.dayofweek)
final_cost_rev_df['day'] = final_cost_rev_df['day'].map(dmap)

# Creating profit column calculation
final_cost_rev_df['profit'] = round(final_cost_rev_df['revenue'] - final_cost_rev_df['cost'], 2)
# Change the columns order
final_cost_rev_df = final_cost_rev_df[['date','day', 'site', 'vertical', 'cost', 'revenue', 'profit']]

# Creating the sem costs + data rev table
sem_cost_rev = pd.concat([sem_costs_df, data_rev_df], sort=True).groupby(by=['date', 'site', 'source', 'vertical']).agg({'clicks':'sum', 'revenue':'sum', 'cost':'sum'}).reset_index()

# Create the profit and the day columns
sem_cost_rev['profit'] = round(sem_cost_rev['revenue'] - sem_cost_rev['cost'], 2)
sem_cost_rev['day'] = sem_cost_rev['date'].apply(lambda x: x.dayofweek)
sem_cost_rev['day'] = sem_cost_rev['day'].map(dmap)

# Create the final revenue table only
final_data_rev = data_rev_df
final_data_rev['value per click'] = round(final_data_rev['revenue']/final_data_rev['clicks'], 2)
final_data_rev['day'] = final_data_rev['date'].apply(lambda x: x.dayofweek)
final_data_rev['day'] = final_data_rev['day'].map(dmap)


# Saving the files as csv
final_cost_rev_df.to_csv(path + 'cost_rev.csv', index=False)
sem_cost_rev.to_csv(path + 'sem_cost_rev.csv', index=False)
final_data_rev.to_csv(path + 'final_data_rev.csv', index=False)

# creating a connection to bigQuery
proj_id = 'daily-reports-252312'


# upload the data frames
sem_cost_rev.to_gbq(destination_table='rev_and_cost.sem_cost_rev', project_id=proj_id, if_exists='replace', chunksize=10000)
final_cost_rev_df.to_gbq(destination_table='rev_and_cost.cost_rev', project_id=proj_id, if_exists='replace', chunksize=10000)

## create a final data rev table and upload
final_data_rev_to_gbq = final_data_rev.rename(columns={'value per click': 'value_per_click'})
final_data_rev_to_gbq.to_gbq(destination_table='rev_and_cost.final_data_rev', project_id=proj_id, if_exists='replace', chunksize=10000)

# uploading the backup files
sem_cost_rev.to_gbq(destination_table='backup.sem_cost_rev', project_id=proj_id, if_exists='replace')
final_cost_rev_df.to_gbq(destination_table='backup.final_cost_rev', project_id=proj_id, if_exists='replace')
final_data_rev_to_gbq.to_gbq(destination_table='backup.final_data_rev', project_id=proj_id, if_exists='replace')

sem_cost_rev_file_rows = sem_cost_rev.shape[0]
cost_rev_file_rows = final_cost_rev_df.shape[0]
final_data_rev_file_rows = final_data_rev_to_gbq.shape[0]

sem_cost_rev_bq_rows = pandas_gbq.read_gbq('SELECT count(*) from rev_and_cost.sem_cost_rev', project_id=proj_id)
sem_cost_rev_bq_rows = int(sem_cost_rev_bq_rows.iloc[0])
cost_rev_bq_rows = pandas_gbq.read_gbq('SELECT count(*) from rev_and_cost.cost_rev', project_id=proj_id)
cost_rev_bq_rows = int(cost_rev_bq_rows.iloc[0])
final_data_rev_bq_rows = pandas_gbq.read_gbq('SELECT count(*) from rev_and_cost.final_data_rev', project_id=proj_id)
final_data_rev_bq_rows = int(final_data_rev_bq_rows.iloc[0])

valid_rows(sem_cost_rev_file_rows, sem_cost_rev_bq_rows)
valid_rows(cost_rev_file_rows, cost_rev_bq_rows)
valid_rows(final_data_rev_file_rows, final_data_rev_bq_rows)

## check for duplicates

tables = ['rev_and_cost.cost_rev', 'rev_and_cost.sem_cost_rev', 'rev_and_cost.final_data_rev']
cost_rev_dup_query = 'select date, site, vertical, count(*) from ' + tables[0] + ' group by date, site, vertical having count(*) > 1'
sem_cost_rev_dup_query = 'select date, site, source, vertical, count(*) from ' + tables[1] + ' group by date, site, source, vertical having count(*) > 1'
data_rev_dup_query = 'SELECT date, advertiser, site, source, vertical, count(*) from ' + tables[2] + ' group by date, advertiser, site, source, vertical having count(*) > 1'

dup_rows_validation(cost_rev_dup_query, tables[0], proj_id)
dup_rows_validation(sem_cost_rev_dup_query, tables[1], proj_id)
#dup_rows_validation(data_rev_dup_query, tables[2], proj_id)


